const pkg = require('./package')

module.exports = {
  mode: 'universal',

  /*
   ** Headers of the page
   */
  head: {
    title: pkg.name,
    meta: [{
        charset: 'utf-8'
      },
      {
        name: 'viewport',
        content: 'width=device-width, initial-scale=1'
      },
      {
        hid: 'description',
        name: 'description',
        content: pkg.description
      }
    ],
    link: [{
      rel: 'icon',
      type: 'image/x-icon',
      href: '/favicon.ico'
    }]
  },

  /*
   ** Customize the progress-bar color
   */
  loading: {
    color: '#fff'
  },

  /*
   ** Global CSS
   */
  css: [],

  /*
   ** Plugins to load before mounting the App
   */
  plugins: [
    '~/plugins/components',
    '~/plugins/filters'
  ],

  /*
   ** Nuxt.js modules
   */
  modules: [
    ['storyblok-nuxt', {
      accessToken: 'sdP0KvQO8ltuQCCh2a793gtt',
      cacheProvider: 'memory'
    }],
    '@nuxtjs/style-resources'
  ],

  styleResources: {
    scss: ['~assets/main.scss'],  // That file @includes stuff from node_modules
  },

  /*
   ** Router middleware
   */
  router: {
    middleware: 'setCacheVersion'
  },

  /*
   ** Build configuration
   */
  build: {
    /*
     ** You can extend webpack config here
     */
    postcss: [
      require('tailwindcss')('./tailwind.js'),
      require('autoprefixer')
    ],
    extend(config, ctx) {

    }
  },
  css: ['~/assets/css/tailwind.css']
}
