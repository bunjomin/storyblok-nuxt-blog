import Vuex from 'vuex'

const createStore = () => {
  return new Vuex.Store({
    state: {
      cacheVersion: '',
      articles: {}
    },
    mutations: {
      setCacheVersion (state, version) {
        state.cacheVersion = version
      },
      setLatestArticles (state, articles) {
        state.articles = articles
      }
    },
    actions: {
      loadCacheVersion ({ commit }) {
        return this.$storyapi.get(`cdn/spaces/me`).then((res) => {
          commit('setCacheVersion', res.data.space.version)
        })
      },
      loadLatestArticles ({ commit }) {
        return this.$storyapi.get(`cdn/stories?starts_with=articles/&sort_by=content.published_at:desc/&per_page=3`).then(res => {
          commit('setLatestArticles', res.data.stories)
        })
      }
    }
  })
}

export default createStore
