import Vue from 'vue'
import Page from '~/components/Page.vue'
import Article from '~/components/Article.vue'
import Hero from '~/components/Hero.vue'
import Grid from '~/components/Grid.vue'
import Feature from '~/components/Feature.vue'
import LatestArticles from '~/components/LatestArticles.vue'

Vue.component('blok-page', Page)
Vue.component('blok-article', Article)
Vue.component('blok-latest-articles', LatestArticles)
Vue.component('blok-hero', Hero)
Vue.component('blok-grid', Grid)
Vue.component('blok-feature', Feature)
